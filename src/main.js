var texts = {
  font: null,
  alphabet: '1234567890QWERTYUIOPASDFGHJKLZXCVBNM, .',
  alpha: '1234567890QWERTYUIOPASDFGHJKLZXCVBNM,.',
  coordinates: [
    [-4.5, -2], // 1
    [-3.5, -2], // 2
    [-2.5, -2], // 3
    [-1.5, -2], // 4
    [-0.5, -2], // 5
    [0.5, -2], // 6
    [1.5, -2], // 7
    [2.5, -2], // 8
    [3.5, -2], // 9
    [4.5, -2], // 0
    [-4.5, -1], // Q
    [-3.5, -1], // W
    [-2.5, -1], // E
    [-1.5, -1], // R
    [-0.5, -1], // T
    [0.5, -1], // Y
    [1.5, -1], // U
    [2.5, -1], // I
    [3.5, -1], // O
    [4.5, -1], // P
    [-4, 0], // A
    [-3, 0], // S
    [-2, 0], // D
    [-1, 0], // F
    [0, 0], // G
    [1, 0], // H
    [2, 0], // J
    [3, 0], // K
    [4, 0], // L
    [-3, 1], // Z
    [-2, 1], // X
    [-1, 1], // C
    [0, 1], // V
    [1, 1], // B
    [2, 1], // N
    [3, 1], // M
    [-3, 2], // ,
    [0, 2], // space
    [3, 2] // .
  ],
  intro: 'If you are using gesture typing, you hunt for the initial letter of the wanted words, then you slide through all the following letters. If you do it enough times, that gestures sticks in your motor memory, it becomes automatic and involuntary. Though deciphering gestures is another question.',
  split: null,
  type: 'Type something!',
  typed: ''
}
texts.split = texts.intro.split(' ')
var introArray = create2DArray(6, 8, 0, true)
var clickArray = create2DArray(6, 8, 0, true)
var editorArray = create2DArray(6, 11, 0, true)
var checkArray = create2DArray(6, 11, 0, true)
var stringArray = create2DArray(6, 11, 0, true)
for (var i = 0; i < texts.split.length; i++) {
  texts.split[i] = texts.split[i][0] + texts.split[i] + texts.split[i][texts.split[i].length - 1]
}
var coordinates = []
var tempCoords = []
for (var i = 0; i < texts.split.length; i++) {
  for (var j = 0; j < texts.split[i].length; j++) {
    for (var k = 0; k < texts.alphabet.length; k++) {
      if (texts.alphabet[k] === texts.split[i][j].toUpperCase()) {
        tempCoords.push(texts.coordinates[k])
      }
    }
  }
  coordinates.push(tempCoords)
  tempCoords = []
}
for (var i = 0; i < introArray.length; i++) {
  for (var j = 0; j < introArray[i].length; j++) {
    introArray[i][j] = coordinates[j * introArray.length + i]
  }
}
var points = []
var counter = 0
var colors = {
  white: [255, 255, 255],
  black: [0, 0, 0]
}
var states = {
  intro: true,
  editor: false,
  typed: false
}
var board = {
  size: null
}

function preload() {
  texts.font = loadFont('ttf/piazzolla.ttf')
}

function setup() {
  if (windowWidth >= windowHeight) {
    board.size = windowHeight * 0.9
  } else {
    board.size = windowWidth * 0.9
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  background(255)
  rectMode(CENTER)
  textAlign(LEFT, CENTER)
  textFont(texts.font)
  colorMode(RGB, 255, 255, 255, 1)

  if (states.intro === true) {
    // drawing curves
    for (var i = 0; i < introArray.length; i++) {
      for (var j = 0; j < introArray[i].length; j++) {
        if (mouseX >= windowWidth * 0.5 + (i - introArray.length * 0.5 + 0.5) * board.size * 0.16 - board.size * 0.08 && mouseX <= windowWidth * 0.5 + (i - introArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.08 && mouseY >= windowHeight * 0.5 + (j - introArray[i].length * 0.5 + 0.5) * board.size * 0.085 - board.size * 0.5 * 0.085 && mouseY <= windowHeight * 0.5 + (j - introArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.5 * 0.085 || clickArray[i][j] === 1) {
          textAlign(CENTER, CENTER)
          textSize(board.size * 0.025)
          fill(colors.black)
          noStroke()
          text(texts.intro.split(' ')[j * introArray.length + i], windowWidth * 0.5 + (i - introArray.length * 0.5 + 0.5) * board.size * 0.16, windowHeight * 0.5 + (j - introArray[i].length * 0.5 + 0.5) * board.size * 0.085 - board.size * 0.005)
        } else if (clickArray[i][j] === 0) {
          noFill()
          stroke(colors.black)
          strokeWeight(1)
          curveTightness(1.25)
          beginShape()
          for (var k = 0; k < introArray[i][j].length; k++) {
            curveVertex(windowWidth * 0.5 + introArray[i][j][k][0] * board.size * 0.16 * 0.1 + board.size * 0.16 * (i - introArray.length * 0.5 + 0.5), windowHeight * 0.5 + introArray[i][j][k][1] * board.size * 0.16 * 0.1 + board.size * 0.085 * (j - introArray[i].length * 0.5 + 0.5))
          }
          endShape()
        }
      }
    }
    // drawing points
    for (var i = 0; i < introArray.length; i++) {
      for (var j = 0; j < introArray[i].length; j++) {
        for (var k = 0; k < introArray[i][j].length; k++) {
          if (mouseX >= windowWidth * 0.5 + (i - introArray.length * 0.5 + 0.5) * board.size * 0.16 - board.size * 0.08 && mouseX <= windowWidth * 0.5 + (i - introArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.08 && mouseY >= windowHeight * 0.5 + (j - introArray[i].length * 0.5 + 0.5) * board.size * 0.085 - board.size * 0.5 * 0.085 && mouseY <= windowHeight * 0.5 + (j - introArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.5 * 0.085) {

          } else if (clickArray[i][j] === 0) {
            noFill()
            stroke(colors.black)
            strokeWeight(3)
            point(windowWidth * 0.5 + (i - introArray.length * 0.5 + 0.5) * board.size * 0.16 + introArray[i][j][k][0] * board.size * 0.16 * 0.1, windowHeight * 0.5 + (j - introArray[i].length * 0.5 + 0.5) * board.size * 0.085 + introArray[i][j][k][1] * board.size * 0.16 * 0.1)
          }
        }
      }
    }
  }
  if (states.editor === true) {
    // display text
    if (states.typed === false) {
      textAlign(CENTER, CENTER)
      fill(colors.black)
      noStroke()
      textSize(board.size * 0.05)
      text(texts.type, windowWidth * 0.5, windowHeight * 0.5)
    }
    // drawing background glyphs
    for (var i = 0; i < editorArray.length; i++) {
      for (var j = 0; j < editorArray[i].length; j++) {
        if (editorArray[i][j] !== 0) {
          if (mouseX >= windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16 - board.size * 0.08 && mouseX <= windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.08 && mouseY >= windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 - board.size * 0.5 * 0.085 && mouseY <= windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.5 * 0.085) {

          } else if (checkArray[i][j] === 0) {
            noFill()
            stroke(colors.black)
            strokeWeight(3)
            for (var k = 0; k < editorArray[i][j].length; k++) {
              point(windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16 + editorArray[i][j][k][0] * board.size * 0.16 * 0.1, windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 + editorArray[i][j][k][1] * board.size * 0.16 * 0.1)
            }
          }

          if (mouseX >= windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16 - board.size * 0.08 && mouseX <= windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.08 && mouseY >= windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 - board.size * 0.5 * 0.085 && mouseY <= windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.5 * 0.085 || checkArray[i][j] === 1) {
            textAlign(CENTER, CENTER)
            textSize(board.size * 0.025)
            fill(colors.black)
            noStroke()
            text(stringArray[i][j], windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16, windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 - board.size * 0.005)
          } else if (checkArray[i][j] === 0) {
            noFill()
            stroke(colors.black)
            strokeWeight(1)
            curveTightness(1.25)
            beginShape()
            curveVertex(windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.16 * 0.1 * editorArray[i][j][0][0], windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.16 * 0.1 * editorArray[i][j][0][1])
            for (var l = 0; l < editorArray[i][j].length; l++) {
              curveVertex(windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.16 * 0.1 * editorArray[i][j][l][0], windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.16 * 0.1 * editorArray[i][j][l][1])
            }
            curveVertex(windowWidth * 0.5 + (i - editorArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.16 * 0.1 * editorArray[i][j][editorArray[i][j].length - 1][0], windowHeight * 0.5 + (j - editorArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.16 * 0.1 * editorArray[i][j][editorArray[i][j].length - 1][1])
            endShape()
          }
        }
      }
    }
    // drawing foreground glyph in progress
    for (var i = 0; i < points.length; i++) {
      noFill()
      stroke(colors.black)
      strokeWeight(3)
      point(windowWidth * 0.5 + points[i][0] * board.size * 0.08, windowHeight * 0.5 + (points[i][1] + 1) * board.size * 0.08)

      noFill()
      stroke(colors.black)
      strokeWeight(1)
      curveTightness(1.25)
      beginShape()
      curveVertex(windowWidth * 0.5 + board.size * 0.08 * points[0][0], windowHeight * 0.5 + board.size * 0.08 * (points[0][1] + 1))
      for (var j = 0; j < points.length; j++) {
        curveVertex(windowWidth * 0.5 + board.size * 0.08 * points[j][0], windowHeight * 0.5 + board.size * 0.08 * (points[j][1] + 1))
      }
      curveVertex(windowWidth * 0.5 + board.size * 0.08 * points[points.length - 1][0], windowHeight * 0.5 + board.size * 0.08 * (points[points.length - 1][1] + 1))
      endShape()
    }
  }
  // reset counter
  if (counter === 66) {
    counter = 0
  }
}

function mousePressed() {
  if (states.intro === true) {
    for (var i = 0; i < clickArray.length; i++) {
      for (var j = 0; j < clickArray[i].length; j++) {
        if (mouseX >= windowWidth * 0.5 + (i - clickArray.length * 0.5 + 0.5) * board.size * 0.16 - board.size * 0.08 && mouseX <= windowWidth * 0.5 + (i - clickArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.08 && mouseY >= windowHeight * 0.5 + (j - clickArray[i].length * 0.5 + 0.5) * board.size * 0.085 - board.size * 0.5 * 0.085 && mouseY <= windowHeight * 0.5 + (j - clickArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.5 * 0.085) {
          if (clickArray[i][j] === 0) {
            clickArray[i][j] = 1
          } else {
            clickArray[i][j] = 0
          }
        }
      }
    }
    if (mouseX <= windowWidth * 0.5 - board.size * 0.5 || mouseX >= windowWidth * 0.5 + board.size * 0.5 || mouseY <= windowHeight * 0.5 - board.size * 0.5 || mouseY >= windowHeight * 0.5 + board.size * 0.5) {
      states.intro = false
      states.editor = true
    }
  }
  if (states.editor === true) {
    for (var i = 0; i < checkArray.length; i++) {
      for (var j = 0; j < checkArray[i].length; j++) {
        if (mouseX >= windowWidth * 0.5 + (i - checkArray.length * 0.5 + 0.5) * board.size * 0.16 - board.size * 0.08 && mouseX <= windowWidth * 0.5 + (i - checkArray.length * 0.5 + 0.5) * board.size * 0.16 + board.size * 0.08 && mouseY >= windowHeight * 0.5 + (j - checkArray[i].length * 0.5 + 0.5) * board.size * 0.085 - board.size * 0.5 * 0.085 && mouseY <= windowHeight * 0.5 + (j - checkArray[i].length * 0.5 + 0.5) * board.size * 0.085 + board.size * 0.5 * 0.085) {
          if (checkArray[i][j] === 0) {
            checkArray[i][j] = 1
          } else {
            checkArray[i][j] = 0
          }
        }
      }
    }
  }
}

function keyPressed() {
  if (states.editor === true) {
    for (var i = 0; i < texts.alpha.length; i++) {
      if (key.toUpperCase() === texts.alpha[i]) {
        texts.typed = texts.typed + key
        points.push(texts.coordinates[i])
        states.typed = true
      }
    }
    if (key === ' ') {
      if (counter < 66) {
        editorArray[counter % editorArray.length][Math.floor(counter / editorArray.length)] = points
        stringArray[counter % stringArray.length][Math.floor(counter / stringArray.length)] = texts.typed
        counter++
      }
      texts.typed = ''
      points = []
    }
    if (keyCode === 8) {
      texts.typed = texts.typed.slice(0, -1)
      points.pop()
    }
  }
  return false
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    board.size = windowHeight * 0.9
  } else {
    board.size = windowWidth * 0.9
  }
  createCanvas(windowWidth, windowHeight)
}
